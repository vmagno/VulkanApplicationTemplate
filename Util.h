#ifndef UTIL_H
#define UTIL_H

#include <fstream>
#include <vector>

namespace Util {

/**
 * @brief Read the content of a file into a char buffer
 * @param filename The file to read
 * @return A vector with the content of the file
 */
std::vector<char> readFile(const std::string& filename);

} // namespace Util

#endif // UTIL_H
