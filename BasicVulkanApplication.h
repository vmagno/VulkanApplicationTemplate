#ifndef BASIC_VULKAN_APPLICATION_H
#define BASIC_VULKAN_APPLICATION_H

#include <iostream>
#include <limits>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
// Vulkan depth buffer is between 0 and 1 (not like OpenGL -1 to 1)
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "HighResolutionTimer.h"

class BasicVulkanApplication
{
    struct QueueFamilyIndices;
    struct SwapChainSupportDetails;

public:
    BasicVulkanApplication()
        : window_(nullptr)
        , width_(DEFAULT_WIDTH)
        , height_(DEFAULT_HEIGHT)
        , physicalDevice_(VK_NULL_HANDLE)
        , currentFrame_(0)
        , hasResizedFramebuffer_(false)
        , frameCount_(0)
        , currentFps_(0.f)
    {
    }

    /**
     * @brief Run the whole application, from initialization to cleanup
     */
    void run()
    {
        initWindow();
        initVulkan();
        mainLoop();
        cleanup();
    }

private:
    GLFWwindow* window_; //!< GLFW Window that appears on the monitor
    int32_t     width_;  //!< Width of the window in pixels
    int32_t     height_; //!< Height of the window in pixels

    // Vulkan objects
    VkInstance               instance_;      //!< Application states, interface with Vulkan
    VkDebugReportCallbackEXT debugCallback_; //!< Function called by the validation layers
    VkSurfaceKHR             windowSurface_; //!< Abstract platform-specific surface to which to present rendered images
    VkPhysicalDevice         physicalDevice_; //!< Handle to the GPU that will display the application
    VkDevice logicalDevice_; //!< To interface with the physical device. Can have more than one per physical device
    VkQueue  graphicsQueue_; //!< Handle to the graphics queue instance created along with the (logical) device
    VkQueue  presentQueue_;  //!< Handle to the present queue instance created along with the (logical) device
    VkSwapchainKHR               swapChain_;             //!< Buffer infrastructure of images waiting to be presented
    std::vector<VkImage>         swapChainImages_;       //!< Handles to images contained in the swap chain
    VkFormat                     swapChainImageFormat_;  //!< Remember the format used when creating the swap chain
    VkExtent2D                   swapChainExtent_;       //!< Remember the extent used when creating the swap chain
    std::vector<VkImageView>     swapChainImageViews_;   //!< How to access the images in the swap chain
    std::vector<VkFramebuffer>   swapChainFramebuffers_; //!< To bind the image attachments from the render pass
    VkRenderPass                 renderPass_; //!< Specifies the details of the framebuffers used while rendering
    VkDescriptorSetLayout        descriptorSetLayout_; //!< To pass uniform values to shaders
    VkPipelineLayout             pipelineLayout_;      //!< To configure uniform variables
    VkPipeline                   graphicsPipeline_;    //!< Organization of stages, their layout and the render pass
    VkCommandPool                commandPool_;         //!< To manage memory used by the buffers and command buffers
    VkImage                      depthImage_;          //!< Will be the depth buffer
    VkDeviceMemory               depthImageMemory_;
    VkImageView                  depthImageView_;
    VkImage                      textureImage_;
    VkDeviceMemory               textureImageMemory_;
    VkImageView                  textureImageView_;
    VkSampler                    textureSampler_;
    VkBuffer                     vertexBuffer_;       //!< To hold vertex data on the GPU
    VkDeviceMemory               vertexBufferMemory_; //!< Memory where the vertex data will be stored (why different?)
    VkBuffer                     indexBuffer_;        //!< To hold index data on the GPU
    VkDeviceMemory               indexBufferMemory_;  //!< Memory where index data will be stored
    std::vector<VkBuffer>        uniformBuffers_; //!< For passing uniforms to shaders. Need one per swap chain image
    std::vector<VkDeviceMemory>  uniformBuffersMemory_; //!< Memory where the uniforms will be stored
    VkDescriptorPool             descriptorPool_;       //!< From which descriptor sets will be allocated
    std::vector<VkDescriptorSet> descriptorSets_;       //!< To bind the uniform buffers
    std::vector<VkCommandBuffer> commandBuffers_;       //!< To record drawing commands

    // Synch objects
    std::vector<VkSemaphore> imageAvailableSemaphores_;
    std::vector<VkSemaphore> renderFinishedSemaphores_;
    std::vector<VkFence>     inFlightFences_;
    uint32_t                 currentFrame_;

    bool hasResizedFramebuffer_;

    // Timers
    Util::HighResolutionTimer frameTimer_;
    Util::HighResolutionTimer fpsTimer_;
    uint64_t                  frameCount_;
    float                     currentFps_;

    static const unsigned int DEFAULT_WIDTH  = 800;
    static const unsigned int DEFAULT_HEIGHT = 600;

    static const uint32_t INVALID = std::numeric_limits<uint32_t>::max(); //!< Value that describes an invalid uint32_t

    /// Maximum number of frames that can be "in flight" (work submitted by CPU, but not processed by GPU yet)
    static const uint32_t MAX_FRAMES_IN_FLIGHT = 2;

private: // functions
    void initWindow();
    void initVulkan();
    void mainLoop();
    void cleanup();

    //! \name Initialization sequence
    //!@{
    /**
     * @brief Create the Vulkan instance that will hold the application state.
     */
    void createInstance();

    /**
     * @brief Setup the callback function that will print out errors from the validation layers.
     */
    void setupDebugCallback();

    /**
     * @brief Create the surface that will be used to present images to the system window.
     */
    void createSurface();

    /**
     * @brief Loop through the available devices on the system and select the most/first appropriate one.
     */
    void pickPhysicalDevice();

    /**
     * @brief Create the logical device that will be used to interface with the physical one.
     */
    void createLogicalDevice();

    /**
     * @brief Create the buffer infrastructure of images to be presented (not the buffer themselves).
     */
    void createSwapChain();

    /**
     * @brief Create the interface (view) to every image in the swap chain (not the images).
     */
    void createImageViews();

    /**
     * @brief Create the structure/details of framebuffers that will be used while rendering (not the framebuffers
     * themselves).
     */
    void createRenderPass();

    /**
     * @brief Create the binding used to pass the UniformBufferObject to the graphics pipeline
     */
    void createDescriptorSetLayout();

    /**
     * @brief Create the organization of rendering stages (both programmable and fixed ones), their layout and the
     * render pass.
     */
    void createGraphicsPipeline();

    /**
     * @brief Create the framebuffers that bind the image used as attachments in the render pass.
     */
    void createFramebuffers();

    /**
     * @brief Create the pool that will manage the memory used by command buffers and from which these buffers will be
     * allocated.
     */
    void createCommandPool();

    void createDepthResources();

    void createTextureImage();
    void createTextureImageView();
    void createTextureSampler();

    /**
     * @brief Create the buffer used to hold vertex data.
     */
    void createVertexBuffer();

    /**
     * @brief Create the buffer used to hold index data.
     */
    void createIndexBuffer();

    /**
     * @brief Create the buffers used to hold uniform data.
     */
    void createUniformBuffers();

    /**
     * @brief Create the pool from which to allocate the descriptor sets.
     */
    void createDescriptorPool();

    void createDescriptorSets();

    /**
     * @brief Create the buffers in which draw commands will be recorded before being executed by Vulkan.
     */
    void createCommandBuffers();

    /**
     * @brief Create the semaphores and fences that will be used to synchronize the GPU and CPU respectively.
     */
    void createSyncObjects();
    //!@}

    /**
     * @brief Perform the steps needed to actually draw a frame based on the previously done setup.
     */
    void drawFrame();

    /**
     * @brief Update uniform buffer data for the current image. This is responsible for the animation (for now)
     * @param currentImage ID of the image we're processing
     */
    void updateUniformBuffer(uint32_t currentImage);

    /**
     * @brief Re-create the swap chain and everything that depends on it, as well as other components that depend on
     * window size.
     */
    void recreateSwapChain();

    /**
     * @brief Cleanup the swap chain and the components that depend on it (the ones that are recreated in
     * recreateSwapChain().
     */
    void cleanupSwapChain();

    /**
     * @brief Check whether the given device fulfills the criteria needed by the application
     * @param device The physical device to check
     * @return Whether the device can be used by the application
     */
    bool isDeviceSuitable(const VkPhysicalDevice device) const;

    /**
     * @brief Find the ID of each needed queue family (they may overlap) on the given device and return them in a
     * struct.
     * @param device The physical device on which to check for queue family availability
     * @return The set of indices of required queue families
     */
    const QueueFamilyIndices findSuitableQueueFamilies(const VkPhysicalDevice device) const;

    /**
     * @brief Verify whether the given device supports swap chains (to present images to the screen. Some devices might
     * not have any graphics capability). Might not be strictly necessary if we already checked for the presence of a
     * presentation queue family.
     * @param device The physical device queried
     * @return A struct containing the support details
     */
    const SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) const;

    /// Check Vulkan library for support for the validation layers listed in a global variable
    static bool checkValidationLayerSupport();

    /// Check the given device for support for the extensions listed in a global variable
    static bool checkDeviceExtensionSupport(const VkPhysicalDevice device);

    /**
     * @brief Get extensions required by GLFW to properly work with Vulkan. This includes the Window System Interface
     * (WSI) ones, to be able to present rendered images to an actual operating system window. It also includes the swap
     * chain extension, which is also tied to the platform where we are running.
     * @return A list of required extension names
     */
    static std::vector<const char*> getRequiredExtensions();

    /**
     * @brief Choose the format in which colors are stored in a surface
     * @param availableFormats Available formats, as queried by querySwapChainSupport()
     * @return The format to be used when creating the swap chain
     */
    static const VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);

    /**
     * @brief Choose the best available present mode.
     * @param availablePresentModes Available present modes, as queried by querySwapChainSupport()
     * @return The present mode to be used when creating the swap chain
     */
    static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);

    /**
     * @brief Choose the most appropriate extent for the swap chain (*could* be different from window resolution)
     * @param capabilities Contains current extent as well as min and max possible extents
     * @return The most appropriate extent (as far as we know)
     */
    const VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

    /**
     * @brief Finds whether the (physical) device supports the given formats
     * @param candidates Formats that are desired (ordered by preference)
     * @return The first format in [candidates] that is supported by the device
     */
    VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling,
                                 VkFormatFeatureFlags features);

    VkFormat findDepthFormat();

    static inline bool hasStencilComponent(VkFormat format)
    {
        return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
    }

    /**
     * @brief Find the index of a memory type that has the right type and properties
     * @param typeFilter The memory type we want
     * @param properties The properties we are looking for
     * @return The index of a suitable memory type. Will throw an exception if none is found.
     */
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const;

    VkCommandBuffer beginSingleTimeCommands();
    void            endSingleTimeCommands(VkCommandBuffer commandBuffer);

    void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
                     VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);

    /**
     * @brief Transition an image from one layout to another using a barrier (only some specific transitions are
     * implemented here)
     * @param image The image to transition
     * @param format Format of the image
     * @param oldLayout Current layout
     * @param newLayout Layout to which we want to transition
     */
    void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

    VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);

    /**
     * @brief Create a buffer on the GPU and allocate memory for it.
     * @param size Size of the buffer in bytes
     * @param usage Buffer usage flags
     * @param properties Desired properties
     * @param[out] buffer Handle to the newly-created buffer
     * @param[out] bufferMemory Handle to the allocated memory
     */
    void createBuffer(const VkDeviceSize size, const VkBufferUsageFlags usage, const VkMemoryPropertyFlags properties,
                      VkBuffer& buffer, VkDeviceMemory& bufferMemory);

    /**
     * @brief Copy data from one device buffer to another
     * @param srcBuffer Source of data
     * @param dstBuffer Destination where to send the data
     * @param size Size of the data in bytes
     */
    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

    void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);

    /**
     * @brief Create a shader module with the given shader binary code
     * @param shaderCode Shader binary code (as an array of char)
     * @return The shader module
     */
    VkShaderModule createShaderModule(const std::vector<char>& shaderCode);

    /// Function called by the validation layers when an error occurs
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
                                                        uint64_t obj, size_t location, int32_t code,
                                                        const char* layerPrefix, const char* msg, void* userData);

    /**
     * @brief We want to create a debug report callback object, but the create() function is not loaded, so we need to
     * first find its address. This is done by calling vkGetInstanceProcAddr(). We then directly call the function (if
     * successful) and return the result, or return an error.
     * @param instance Vulkan instance for which to create the callback
     * @param pCreateInfo Debug callback create info
     * @param pAllocator Debug callback allocator
     * @param pCallback The function to call as callback
     * @return The result of the callback creation function or an error code (if the function was not found)
     */
    static VkResult
        createDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
                                     const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback);

    /**
     * @brief Similarly to createDebugReportCallbackEXT(), the destroy function for the callback is not loaded, so we
     * first need to find it, then call it.
     * @param instance Vulkan instance for which to destroy the callback
     * @param callback The callback to destroy
     * @param pAllocator Debug callback allocator
     */
    static void destroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback,
                                              const VkAllocationCallbacks* pAllocator);

    static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

private: // structs
    /**
     * @brief List of queue family indices that are found on a device and that possess certain characteristics
     */
    struct QueueFamilyIndices
    {
        uint32_t graphicsFamily_; //!< ID of a queue family that has graphics capabilities
        uint32_t presentFamily_; //!< ID of a queue familiy that is able to present images (likely the same as graphics)

        QueueFamilyIndices()
            : graphicsFamily_(INVALID)
            , presentFamily_(INVALID)
        {
        }

        /// Whether all queue families exist
        bool isComplete() const { return (graphicsFamily_ != INVALID && presentFamily_ != INVALID); }
    };

    /**
     * @brief Details of swap chain capabilities on a device.
     */
    struct SwapChainSupportDetails
    {
        VkSurfaceCapabilitiesKHR        capabilities_;
        std::vector<VkSurfaceFormatKHR> formats_;
        std::vector<VkPresentModeKHR>   presentModes_;
    };

    struct UniformBufferObject
    {
        glm::mat4 modelMatrix_;
        glm::mat4 viewMatrix_;
        glm::mat4 projMatrix_;
    };
};

#endif // BASIC_VULKAN_APPLICATION_H
