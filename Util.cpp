#include "Util.h"

namespace Util {

std::vector<char> readFile(const std::string& filename)
{
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open())
    {
        throw std::runtime_error("Failed to open file \"" + filename + "\" !!!");
    }

    // Compute file size in bytes
    auto              fileSize = file.tellg();
    std::vector<char> buffer(static_cast<size_t>(fileSize));

    // Read and close the file
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return buffer;
}

} // namespace Util
