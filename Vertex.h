#ifndef VERTEX_H
#define VERTEX_H

#include <array>

#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

struct Vertex
{
    glm::vec3 position_;
    glm::vec3 color_;
    glm::vec2 texCoord_;

    static VkVertexInputBindingDescription getBindingDescription()
    {
        VkVertexInputBindingDescription bindingDescription = {};

        bindingDescription.binding = 0;
        bindingDescription.stride  = sizeof(Vertex);
        bindingDescription.inputRate =
            VK_VERTEX_INPUT_RATE_VERTEX; // Move to next data entry after each vertex (wrt instance rate)

        return bindingDescription;
    }

    static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions()
    {
        std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};

        attributeDescriptions[0].binding  = 0;
        attributeDescriptions[0].location = 0;                       // location in shader
        attributeDescriptions[0].format   = VK_FORMAT_R32G32B32_SFLOAT; // 2 32-bit floats in position param in shader
        attributeDescriptions[0].offset   = offsetof(Vertex, position_);

        attributeDescriptions[1].binding  = 0;
        attributeDescriptions[1].location = 1;                          // Location in shader
        attributeDescriptions[1].format   = VK_FORMAT_R32G32B32_SFLOAT; // 3 32-bit floats in color param in shader
        attributeDescriptions[1].offset   = offsetof(Vertex, color_);

        attributeDescriptions[2].binding  = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format   = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[2].offset   = offsetof(Vertex, texCoord_);

        return attributeDescriptions;
    }
};

#endif // VERTEX_H
