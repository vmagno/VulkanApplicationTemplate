#include "BasicVulkanApplication.h"

#include <cstring>
#include <set>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include "Util.h"
#include "Vertex.h"
#include "VkUtil.h"

/// Validation layers that we want to activate (when enabled)
const std::vector<const char*> validationLayers = {"VK_LAYER_LUNARG_standard_validation"};
const bool                     enableValidationLayers =
#ifdef NDEBUG
    false
#else
    true
#endif
    ;

/// Device extensions that we want to use
const std::vector<const char*> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

const std::vector<Vertex> objectVertices = {{{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                            {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                            {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                            {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},
                                            {{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                                            {{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
                                            {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
                                            {{-0.5f, 0.5f, -0.5f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}};

const std::vector<uint16_t> objectIndices = {0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4};

void BasicVulkanApplication::initWindow()
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window_ = glfwCreateWindow(static_cast<int>(width_), static_cast<int>(height_), "Vulkan", nullptr, nullptr);

    // Set a resize callback in case the out-of-date flag is not set automatically
    glfwSetFramebufferSizeCallback(window_, framebufferResizeCallback);
    glfwSetWindowUserPointer(window_, this);
}

void BasicVulkanApplication::initVulkan()
{
    createInstance();
    setupDebugCallback();
    createSurface(); // Must be done before physical device selection (may affect the choice)
    pickPhysicalDevice();
    createLogicalDevice();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createDescriptorSetLayout();
    createGraphicsPipeline();
    createCommandPool();
    createDepthResources();
    createFramebuffers();
    createTextureImage();
    createTextureImageView();
    createTextureSampler();
    createVertexBuffer();
    createIndexBuffer();
    createUniformBuffers();
    createDescriptorPool();
    createDescriptorSets();
    createCommandBuffers();
    createSyncObjects();
}

void BasicVulkanApplication::mainLoop()
{
    fpsTimer_.Start();
    while (!glfwWindowShouldClose(window_))
    {
        glfwPollEvents();

        frameTimer_.Start();
        drawFrame();
        frameTimer_.Stop();

        frameCount_++;

        // Estimate FPS
        const float elapsed = fpsTimer_.GetTimeSinceLastStartMs() * 0.001f;
        if (elapsed > 0.8f)
        {
            currentFps_ = frameCount_ / elapsed;
            frameCount_ = 0;
            fpsTimer_.Start();

            glfwSetWindowTitle(window_, std::string("Vulkan " + std::to_string(currentFps_) + " FPS").c_str());
            std::cout << "Average frame time: " << frameTimer_.GetAvgTimeMs() << " ms" << std::endl;
            frameTimer_.Reset();
        }
    }

    checkVk(vkDeviceWaitIdle(logicalDevice_));
}

void BasicVulkanApplication::cleanup()
{
    cleanupSwapChain();

    vkDestroySampler(logicalDevice_, textureSampler_, nullptr);
    vkDestroyImageView(logicalDevice_, textureImageView_, nullptr);
    vkDestroyImage(logicalDevice_, textureImage_, nullptr);
    vkFreeMemory(logicalDevice_, textureImageMemory_, nullptr);

    for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    {
        vkDestroySemaphore(logicalDevice_, renderFinishedSemaphores_[i], nullptr);
        vkDestroySemaphore(logicalDevice_, imageAvailableSemaphores_[i], nullptr);
        vkDestroyFence(logicalDevice_, inFlightFences_[i], nullptr);
    }

    vkDestroyDescriptorPool(logicalDevice_, descriptorPool_, nullptr);
    vkDestroyDescriptorSetLayout(logicalDevice_, descriptorSetLayout_, nullptr);

    for (size_t i = 0; i < swapChainImages_.size(); i++)
    {
        vkDestroyBuffer(logicalDevice_, uniformBuffers_[i], nullptr);
        vkFreeMemory(logicalDevice_, uniformBuffersMemory_[i], nullptr);
    }

    vkDestroyBuffer(logicalDevice_, vertexBuffer_, nullptr);
    vkFreeMemory(logicalDevice_, vertexBufferMemory_, nullptr);
    vkDestroyBuffer(logicalDevice_, indexBuffer_, nullptr);
    vkFreeMemory(logicalDevice_, indexBufferMemory_, nullptr);
    vkDestroyCommandPool(logicalDevice_, commandPool_, nullptr);

    vkDestroyDevice(logicalDevice_, nullptr);
    vkDestroySurfaceKHR(instance_, windowSurface_, nullptr);

    if (enableValidationLayers)
    {
        destroyDebugReportCallbackEXT(instance_, debugCallback_, nullptr);
    }

    vkDestroyInstance(instance_, nullptr);
    glfwDestroyWindow(window_);
    glfwTerminate();
}

void BasicVulkanApplication::createInstance()
{
    if (enableValidationLayers && !checkValidationLayerSupport())
    {
        throw std::runtime_error("Validation layers requested but not available");
    }

    // App info needed by instance info
    VkApplicationInfo appInfo  = {};
    appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName   = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName        = "No engine";
    appInfo.engineVersion      = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion         = VK_API_VERSION_1_1;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo     = &appInfo;

    // Extensions
    auto extensions                    = getRequiredExtensions();
    createInfo.enabledExtensionCount   = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    // Layers
    createInfo.enabledLayerCount = 0;
    if (enableValidationLayers)
    {
        createInfo.enabledLayerCount   = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }

    // Actual instance creation
    checkVk(vkCreateInstance(&createInfo, nullptr, &instance_));
}

void BasicVulkanApplication::setupDebugCallback()
{
    if (!enableValidationLayers)
        return;

    VkDebugReportCallbackCreateInfoEXT createInfo = {};
    createInfo.sType                              = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags                              = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback                        = debugCallback;

    checkVk(createDebugReportCallbackEXT(instance_, &createInfo, nullptr, &debugCallback_));
}

void BasicVulkanApplication::createSurface()
{
    // Create the window surface abstraction. Platform-specific code (that uses Vulkan) is hidden away by this function
    checkVk(glfwCreateWindowSurface(instance_, window_, nullptr, &windowSurface_));
}

void BasicVulkanApplication::pickPhysicalDevice()
{
    // Find out how many devices on this host support Vulkan
    uint32_t deviceCount = 0;
    checkVk(vkEnumeratePhysicalDevices(instance_, &deviceCount, nullptr));

    if (deviceCount == 0)
    {
        throw std::runtime_error("Failed to find a GPU with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> devices(deviceCount);                           // Allocate space for physical devices
    checkVk(vkEnumeratePhysicalDevices(instance_, &deviceCount, devices.data())); // Get the devices

    // Choose an appropriate device
    for (const auto& device : devices)
    {
        if (isDeviceSuitable(device))
        {
            physicalDevice_ = device;
            break;
        }
    }

    if (physicalDevice_ == VK_NULL_HANDLE)
    {
        throw std::runtime_error("Failed to find a suitable GPU!");
    }
}

void BasicVulkanApplication::createLogicalDevice()
{
    // Assemble info that will be used to initialize the device creation info

    // Find the set of unique queues, so that if a queue has more than one of the required capacities, only one queue is
    // created rather than one per capacity
    const QueueFamilyIndices queueFamilyIndices = findSuitableQueueFamilies(physicalDevice_);
    std::set<uint32_t> uniqueQueueFamilies = {queueFamilyIndices.graphicsFamily_, queueFamilyIndices.presentFamily_};

    // Queue creation info
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    const float                          queuePriority = 1.0f;
    for (const uint32_t queueFamily : uniqueQueueFamilies)
    {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex        = queueFamily;
        queueCreateInfo.queueCount              = 1;
        queueCreateInfo.pQueuePriorities        = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    // Device features
    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy        = VK_TRUE;

    // Initialize the actual creation info
    VkDeviceCreateInfo createInfo   = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos    = queueCreateInfos.data();
    createInfo.pEnabledFeatures     = &deviceFeatures;

    createInfo.enabledExtensionCount   = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();

    createInfo.enabledLayerCount = 0;
    if (enableValidationLayers)
    {
        createInfo.enabledLayerCount   = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }

    // Actual logical device creation. This also create the queues
    checkVk(vkCreateDevice(physicalDevice_, &createInfo, nullptr, &logicalDevice_));

    // Retrieve handles to queues. Index = 0 since there is only 1 queue (?? could be more than one)
    // TODO verify that index = 0 thingy
    vkGetDeviceQueue(logicalDevice_, queueFamilyIndices.graphicsFamily_, 0, &graphicsQueue_);
    vkGetDeviceQueue(logicalDevice_, queueFamilyIndices.presentFamily_, 0, &presentQueue_);
}

void BasicVulkanApplication::createSwapChain()
{
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice_);

    // Choose some best parameters
    const VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats_);
    const VkPresentModeKHR   presentMode   = chooseSwapPresentMode(swapChainSupport.presentModes_);
    const VkExtent2D         extent        = chooseSwapExtent(swapChainSupport.capabilities_);

    uint32_t imageCount = swapChainSupport.capabilities_.minImageCount + 1;
    // Make sure we don't ask for more than max (if max is set to more than 0)
    if (swapChainSupport.capabilities_.maxImageCount > 0 && imageCount > swapChainSupport.capabilities_.maxImageCount)
    {
        imageCount = swapChainSupport.capabilities_.maxImageCount;
    }

    // Creation info
    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType                    = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface                  = windowSurface_;
    createInfo.minImageCount            = imageCount;
    createInfo.imageFormat              = surfaceFormat.format;
    createInfo.imageColorSpace          = surfaceFormat.colorSpace;
    createInfo.imageExtent              = extent;
    createInfo.imageArrayLayers         = 1;                                   // 1 unless you want stereo
    createInfo.imageUsage               = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // We render directly to the images

    QueueFamilyIndices qfIndices            = findSuitableQueueFamilies(physicalDevice_);
    uint32_t           queueFamilyIndices[] = {(uint32_t)qfIndices.graphicsFamily_, (uint32_t)qfIndices.presentFamily_};

    // Choose image sharing mode
    if (qfIndices.graphicsFamily_ != qfIndices.presentFamily_)
    {
        createInfo.imageSharingMode      = VK_SHARING_MODE_CONCURRENT; // Can share without thinking about synching
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices   = queueFamilyIndices;
    }
    else
    {
        createInfo.imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE; // Only one queue, so use the more efficient mode
        createInfo.queueFamilyIndexCount = 0;                         // Optional
        createInfo.pQueueFamilyIndices   = nullptr;                   // Optional
    }

    createInfo.preTransform   = swapChainSupport.capabilities_.currentTransform; // Don't apply any transform on images
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;               // Don't blend with other windows
    createInfo.presentMode    = presentMode;
    createInfo.clipped        = VK_TRUE;        // Don't care about pixels when they are occluded by another window
    createInfo.oldSwapchain   = VK_NULL_HANDLE; // Assume we only ever create one swap chain

    // Actual swap chain creation
    checkVk(vkCreateSwapchainKHR(logicalDevice_, &createInfo, nullptr, &swapChain_));

    // Retrieve image handles
    vkGetSwapchainImagesKHR(logicalDevice_, swapChain_, &imageCount, nullptr);
    swapChainImages_.resize(imageCount);
    vkGetSwapchainImagesKHR(logicalDevice_, swapChain_, &imageCount, swapChainImages_.data());

    // Remember some variables
    swapChainImageFormat_ = surfaceFormat.format;
    swapChainExtent_      = extent;
}

void BasicVulkanApplication::createImageViews()
{
    swapChainImageViews_.resize(swapChainImages_.size());
    for (size_t i = 0; i < swapChainImages_.size(); i++)
    {
        swapChainImageViews_[i] =
            createImageView(swapChainImages_[i], swapChainImageFormat_, VK_IMAGE_ASPECT_COLOR_BIT);
    }
}

void BasicVulkanApplication::createRenderPass()
{
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format                  = swapChainImageFormat_;
    colorAttachment.samples                 = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp                  = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp                 = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp           = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp          = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout           = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout             = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment            = 0;
    colorAttachmentRef.layout                = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription depthAttachment = {};
    depthAttachment.format                  = findDepthFormat();
    depthAttachment.samples                 = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp                  = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp                 = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp           = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp          = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout           = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout             = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment            = 1;
    depthAttachmentRef.layout                = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass    = {};
    subpass.pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount    = 1;
    subpass.pColorAttachments       = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    // Dependency graph (only 1 subpass, so dependencies are relatively simple)
    VkSubpassDependency dependency = {};
    dependency.srcSubpass          = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass          = 0;
    dependency.srcStageMask        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask       = 0;
    dependency.dstStageMask        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask       = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};

    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType                  = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount        = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments           = attachments.data();
    renderPassInfo.subpassCount           = 1;
    renderPassInfo.pSubpasses             = &subpass;
    renderPassInfo.dependencyCount        = 1;
    renderPassInfo.pDependencies          = &dependency;

    checkVk(vkCreateRenderPass(logicalDevice_, &renderPassInfo, nullptr, &renderPass_));
}

void BasicVulkanApplication::createDescriptorSetLayout()
{
    VkDescriptorSetLayoutBinding uboLayoutBinding = {};
    uboLayoutBinding.binding                      = 0;
    uboLayoutBinding.descriptorType               = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount              = 1;
    uboLayoutBinding.stageFlags                   = VK_SHADER_STAGE_VERTEX_BIT;
    uboLayoutBinding.pImmutableSamplers           = nullptr; // Optional

    VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
    samplerLayoutBinding.binding                      = 1;
    samplerLayoutBinding.descriptorCount              = 1;
    samplerLayoutBinding.descriptorType               = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers           = nullptr;
    samplerLayoutBinding.stageFlags                   = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 2> bindings = {uboLayoutBinding, samplerLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType                           = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount                    = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings                       = bindings.data();

    checkVk(vkCreateDescriptorSetLayout(logicalDevice_, &layoutInfo, nullptr, &descriptorSetLayout_));
}

void BasicVulkanApplication::createGraphicsPipeline()
{
    auto vertShaderCode = Util::readFile("Shaders/BaseShader.vert.spv");
    auto fragShaderCode = Util::readFile("Shaders/BaseShader.frag.spv");

    VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
    VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);

    ////////////////////////
    /// Programmable stages

    // Vertex shader stage
    VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
    vertShaderStageInfo.sType                           = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage                           = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module                          = vertShaderModule;
    vertShaderStageInfo.pName                           = "main";

    // Fragment shader stage
    VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
    fragShaderStageInfo.sType                           = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage                           = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module                          = fragShaderModule;
    fragShaderStageInfo.pName                           = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    /////////////////
    /// Fixed stages

    auto bindingDescription    = Vertex::getBindingDescription();
    auto attributeDescriptions = Vertex::getAttributeDescriptions();

    // Vertex input (format of vertex data: bindings + attributes)
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType                                = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount        = 1;
    vertexInputInfo.pVertexBindingDescriptions           = &bindingDescription; // Optional
    vertexInputInfo.vertexAttributeDescriptionCount      = attributeDescriptions.size();
    vertexInputInfo.pVertexAttributeDescriptions         = attributeDescriptions.data(); // Optional

    // Input assembly: type of primitives and whether to enable primitive restart (for breaking strips)
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology                               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable                 = VK_FALSE;

    // Viewport
    VkViewport viewport = {};
    viewport.x          = 0.0f;
    viewport.y          = 0.0f;
    viewport.width      = static_cast<float>(swapChainExtent_.width);
    viewport.height     = static_cast<float>(swapChainExtent_.height);
    viewport.minDepth   = 0.0f;
    viewport.maxDepth   = 1.0f;

    // Scissor (kind of like a stencil, or a window superimposed on the viewport)
    // Here we just want to draw the whole viewport, so set to the same position and extent
    VkRect2D scissor = {};
    scissor.offset   = {0, 0};
    scissor.extent   = swapChainExtent_;

    // Viewport state (combination of viewports + scissors, can be multiple of each)
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount                     = 1;
    viewportState.pViewports                        = &viewport;
    viewportState.scissorCount                      = 1;
    viewportState.pScissors                         = &scissor;

    // Rasterizer
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable                       = VK_FALSE;              // wrt near + far planes
    rasterizer.rasterizerDiscardEnable                = VK_FALSE;              // Whether to ignore the geometry...
    rasterizer.polygonMode                            = VK_POLYGON_MODE_FILL;  // Filled vs wireframe vs points
    rasterizer.lineWidth                              = 1.0f;                  // In number of fragments
    rasterizer.cullMode                               = VK_CULL_MODE_BACK_BIT; // Front or back or none
    rasterizer.frontFace                              = VK_FRONT_FACE_COUNTER_CLOCKWISE; // Clockwise or counter
    rasterizer.depthBiasEnable                        = VK_FALSE;
    rasterizer.depthBiasConstantFactor                = 0.0f; // Optional
    rasterizer.depthBiasClamp                         = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor                   = 0.0f; // Optional

    // Multisampling (for anti-aliasing)
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType                                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable                  = VK_FALSE;
    multisampling.rasterizationSamples                 = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading                     = 1.0f;     // Optional
    multisampling.pSampleMask                          = nullptr;  // Optional
    multisampling.alphaToCoverageEnable                = VK_FALSE; // Optional
    multisampling.alphaToOneEnable                     = VK_FALSE; // Optional

    // Depth + stencil buffer
    VkPipelineDepthStencilStateCreateInfo depthStencil = {};

    depthStencil.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable       = VK_TRUE;
    depthStencil.depthWriteEnable      = VK_TRUE;
    depthStencil.depthCompareOp        = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds        = 0.0f; // Optional
    depthStencil.maxDepthBounds        = 1.0f; // Optional

    depthStencil.stencilTestEnable = VK_FALSE; // No stencil
    depthStencil.front             = {};       // Optional
    depthStencil.back              = {};       // Optional

    // Color blending
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {}; // Blending settings per framebuffer
    colorBlendAttachment.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable         = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.colorBlendOp        = VK_BLEND_OP_ADD;      // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.alphaBlendOp        = VK_BLEND_OP_ADD;      // Optional

    VkPipelineColorBlendStateCreateInfo colorBlending = {}; // Global blending settings (contains the per-buffer ones)
    colorBlending.sType                               = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable                       = VK_FALSE;
    colorBlending.logicOp                             = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount                     = 1;
    colorBlending.pAttachments                        = &colorBlendAttachment;
    colorBlending.blendConstants[0]                   = 0.0f; // Optional
    colorBlending.blendConstants[1]                   = 0.0f; // Optional
    colorBlending.blendConstants[2]                   = 0.0f; // Optional
    colorBlending.blendConstants[3]                   = 0.0f; // Optional

    // Dynamic states (can be configured dynamically, when drawing)
    VkDynamicState dynamicStates[] = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH};

    VkPipelineDynamicStateCreateInfo dynamicState = {};
    dynamicState.sType                            = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount                = 2;
    dynamicState.pDynamicStates                   = dynamicStates;

    // Pipeline layout (used for uniform variables, that are dynamically specified)
    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType                      = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount             = 1;                     // Optional
    pipelineLayoutInfo.pSetLayouts                = &descriptorSetLayout_; // Optional
    pipelineLayoutInfo.pushConstantRangeCount     = 0;                     // Optional
    pipelineLayoutInfo.pPushConstantRanges        = nullptr;               // Optional

    checkVk(vkCreatePipelineLayout(logicalDevice_, &pipelineLayoutInfo, nullptr, &pipelineLayout_));

    // The pipeline!
    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount                   = 2;
    pipelineInfo.pStages                      = shaderStages;
    pipelineInfo.pVertexInputState            = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState          = &inputAssembly;
    pipelineInfo.pViewportState               = &viewportState;
    pipelineInfo.pRasterizationState          = &rasterizer;
    pipelineInfo.pMultisampleState            = &multisampling;
    pipelineInfo.pDepthStencilState           = &depthStencil;
    pipelineInfo.pColorBlendState             = &colorBlending;
    pipelineInfo.pDynamicState                = nullptr; // Optional
    pipelineInfo.layout                       = pipelineLayout_;
    pipelineInfo.renderPass                   = renderPass_;
    pipelineInfo.subpass                      = 0;
    pipelineInfo.basePipelineHandle           = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex            = -1;             // Optional

    // Actual pipeline creation!
    checkVk(vkCreateGraphicsPipelines(logicalDevice_, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline_));

    // Shader modules are no longer needed
    vkDestroyShaderModule(logicalDevice_, vertShaderModule, nullptr);
    vkDestroyShaderModule(logicalDevice_, fragShaderModule, nullptr);
}

void BasicVulkanApplication::createFramebuffers()
{
    swapChainFramebuffers_.resize(swapChainImageViews_.size());

    for (size_t i = 0; i < swapChainImageViews_.size(); i++)
    {
        std::array<VkImageView, 2> attachments = {swapChainImageViews_[i], depthImageView_};

        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType                   = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass              = renderPass_;
        framebufferInfo.attachmentCount         = static_cast<uint32_t>(attachments.size());
        framebufferInfo.pAttachments            = attachments.data();
        framebufferInfo.width                   = swapChainExtent_.width;
        framebufferInfo.height                  = swapChainExtent_.height;
        framebufferInfo.layers                  = 1;

        checkVk(vkCreateFramebuffer(logicalDevice_, &framebufferInfo, nullptr, &swapChainFramebuffers_[i]));
    }
}

void BasicVulkanApplication::createCommandPool()
{
    const QueueFamilyIndices queueFamilyIndices = findSuitableQueueFamilies(physicalDevice_);

    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex        = queueFamilyIndices.graphicsFamily_;
    poolInfo.flags                   = 0; // Optional

    checkVk(vkCreateCommandPool(logicalDevice_, &poolInfo, nullptr, &commandPool_));
}

void BasicVulkanApplication::createDepthResources()
{
    VkFormat depthFormat = findDepthFormat();

    createImage(swapChainExtent_.width, swapChainExtent_.height, depthFormat, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage_,
                depthImageMemory_);
    depthImageView_ = createImageView(depthImage_, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

    // Only need to transition it once, so can do it here rather than during the render pass
    transitionImageLayout(depthImage_, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
}

void BasicVulkanApplication::createTextureImage()
{
    // Load
    int      texWidth, texHeight, texChannels;
    stbi_uc* pixels = stbi_load("Textures/weeping_angel_512.jpg", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    VkDeviceSize imageSize = static_cast<VkDeviceSize>(texWidth * texHeight * 4);

    if (!pixels)
    {
        throw std::runtime_error("Failed to load texture image!");
    }

    // Copy to device
    VkBuffer       stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer,
                 stagingBufferMemory);

    void* data;
    checkVk(vkMapMemory(logicalDevice_, stagingBufferMemory, 0, imageSize, 0, &data));
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(logicalDevice_, stagingBufferMemory);

    createImage((uint32_t)texWidth, (uint32_t)texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                textureImage_, textureImageMemory_);

    // Prepare image for copy
    transitionImageLayout(textureImage_, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copyBufferToImage(stagingBuffer, textureImage_, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));

    // Prepare image for reading from shader
    transitionImageLayout(textureImage_, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // Clean up
    stbi_image_free(pixels);
    vkDestroyBuffer(logicalDevice_, stagingBuffer, nullptr);
    vkFreeMemory(logicalDevice_, stagingBufferMemory, nullptr);
}

void BasicVulkanApplication::createTextureImageView()
{
    textureImageView_ = createImageView(textureImage_, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
}

void BasicVulkanApplication::createTextureSampler()
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType               = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter           = VK_FILTER_LINEAR;
    samplerInfo.minFilter           = VK_FILTER_LINEAR;

    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy    = 16;

    samplerInfo.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK; // When clamping to border
    samplerInfo.unnormalizedCoordinates = VK_FALSE;                         // Coordinates between 0 and 1
    samplerInfo.compareEnable           = VK_FALSE;
    samplerInfo.compareOp               = VK_COMPARE_OP_ALWAYS;

    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod     = 0.0f;
    samplerInfo.maxLod     = 0.0f;

    checkVk(vkCreateSampler(logicalDevice_, &samplerInfo, nullptr, &textureSampler_));
}

void BasicVulkanApplication::createVertexBuffer()
{
    VkDeviceSize bufferSize = sizeof(objectVertices[0]) * objectVertices.size();

    // The staging buffer is visible to the host, and is used to transfer data to the device-only buffer (which is not
    // visible to the CPU, but has better performance)
    VkBuffer       stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer,
                 stagingBufferMemory);

    void* data;
    // Map buffer memory to CPU-accessible memory, then copy the data
    checkVk(vkMapMemory(logicalDevice_, stagingBufferMemory, 0, bufferSize, 0, &data));
    std::memcpy(data, objectVertices.data(), (size_t)bufferSize);
    vkUnmapMemory(logicalDevice_, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer_, vertexBufferMemory_);

    copyBuffer(stagingBuffer, vertexBuffer_, bufferSize);

    // Clean up staging buffer
    vkDestroyBuffer(logicalDevice_, stagingBuffer, nullptr);
    vkFreeMemory(logicalDevice_, stagingBufferMemory, nullptr);
}

void BasicVulkanApplication::createIndexBuffer()
{
    VkDeviceSize bufferSize = sizeof(objectIndices[0]) * objectIndices.size();

    VkBuffer       stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer,
                 stagingBufferMemory);

    void* data;
    vkMapMemory(logicalDevice_, stagingBufferMemory, 0, bufferSize, 0, &data);
    std::memcpy(data, objectIndices.data(), (size_t)bufferSize);
    vkUnmapMemory(logicalDevice_, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer_, indexBufferMemory_);

    copyBuffer(stagingBuffer, indexBuffer_, bufferSize);

    vkDestroyBuffer(logicalDevice_, stagingBuffer, nullptr);
    vkFreeMemory(logicalDevice_, stagingBufferMemory, nullptr);
}

void BasicVulkanApplication::createUniformBuffers()
{
    VkDeviceSize bufferSize = sizeof(UniformBufferObject);

    uniformBuffers_.resize(swapChainImages_.size());
    uniformBuffersMemory_.resize(swapChainImages_.size());

    for (size_t i = 0; i < swapChainImages_.size(); i++)
    {
        createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                     VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers_[i],
                     uniformBuffersMemory_[i]);
    }
}

void BasicVulkanApplication::createDescriptorPool()
{
    std::array<VkDescriptorPoolSize, 2> poolSizes = {};
    poolSizes[0].type                             = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount                  = static_cast<uint32_t>(swapChainImages_.size());
    poolSizes[1].type                             = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount                  = static_cast<uint32_t>(swapChainImages_.size());

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType                      = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount              = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes                 = poolSizes.data();
    poolInfo.maxSets                    = static_cast<uint32_t>(swapChainImages_.size());

    checkVk(vkCreateDescriptorPool(logicalDevice_, &poolInfo, nullptr, &descriptorPool_));
}

void BasicVulkanApplication::createDescriptorSets()
{
    std::vector<VkDescriptorSetLayout> layouts(swapChainImages_.size(), descriptorSetLayout_);
    VkDescriptorSetAllocateInfo        allocInfo = {};
    allocInfo.sType                              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool                     = descriptorPool_;
    allocInfo.descriptorSetCount                 = static_cast<uint32_t>(swapChainImages_.size());
    allocInfo.pSetLayouts                        = layouts.data();

    descriptorSets_.resize(swapChainImages_.size());
    checkVk(vkAllocateDescriptorSets(logicalDevice_, &allocInfo, &descriptorSets_[0]));

    // Populate the descriptors
    for (size_t i = 0; i < swapChainImages_.size(); i++)
    {
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer                 = uniformBuffers_[i];
        bufferInfo.offset                 = 0;
        bufferInfo.range                  = sizeof(UniformBufferObject);

        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout           = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView             = textureImageView_;
        imageInfo.sampler               = textureSampler_;

        std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};

        descriptorWrites[0].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet          = descriptorSets_[i];
        descriptorWrites[0].dstBinding      = 0;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo     = &bufferInfo;

        descriptorWrites[1].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet          = descriptorSets_[i];
        descriptorWrites[1].dstBinding      = 1;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo      = &imageInfo;

        vkUpdateDescriptorSets(logicalDevice_, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(),
                               0, nullptr);
    }
}

void BasicVulkanApplication::createCommandBuffers()
{
    commandBuffers_.resize(swapChainFramebuffers_.size());

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool                 = commandPool_;
    allocInfo.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount          = static_cast<uint32_t>(commandBuffers_.size());

    checkVk(vkAllocateCommandBuffers(logicalDevice_, &allocInfo, commandBuffers_.data()));

    for (size_t i = 0; i < commandBuffers_.size(); i++)
    {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags                    = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        beginInfo.pInheritanceInfo         = nullptr; // Optional

        checkVk(vkBeginCommandBuffer(commandBuffers_[i], &beginInfo));

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType                 = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass            = renderPass_;
        renderPassInfo.framebuffer           = swapChainFramebuffers_[i];
        renderPassInfo.renderArea.offset     = {0, 0};
        renderPassInfo.renderArea.extent     = swapChainExtent_;

        std::array<VkClearValue, 2> clearValues = {};
        clearValues[0].color                    = {{0.0f, 0.0f, 0.0f, 1.0f}};
        clearValues[1].depthStencil             = {1.0f, 0};
        renderPassInfo.clearValueCount          = static_cast<uint32_t>(clearValues.size());
        renderPassInfo.pClearValues             = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers_[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline_);

        // Bind the vertex buffer
        VkBuffer     vertexBuffers[] = {vertexBuffer_};
        VkDeviceSize offsets[]       = {0};
        vkCmdBindVertexBuffers(commandBuffers_[i], 0, 1, vertexBuffers, offsets);

        vkCmdBindIndexBuffer(commandBuffers_[i], indexBuffer_, 0, VK_INDEX_TYPE_UINT16);

        vkCmdBindDescriptorSets(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout_, 0, 1,
                                &descriptorSets_[i], 0, nullptr);
        vkCmdDrawIndexed(commandBuffers_[i], static_cast<uint32_t>(objectIndices.size()), 1, 0, 0, 0);
        vkCmdEndRenderPass(commandBuffers_[i]);

        checkVk(vkEndCommandBuffer(commandBuffers_[i]));
    }
}

void BasicVulkanApplication::createSyncObjects()
{
    imageAvailableSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores_.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences_.resize(MAX_FRAMES_IN_FLIGHT);

    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags             = VK_FENCE_CREATE_SIGNALED_BIT;

    for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    {
        checkVk(vkCreateSemaphore(logicalDevice_, &semaphoreInfo, nullptr, &imageAvailableSemaphores_[i]));
        checkVk(vkCreateSemaphore(logicalDevice_, &semaphoreInfo, nullptr, &renderFinishedSemaphores_[i]));
        checkVk(vkCreateFence(logicalDevice_, &fenceInfo, nullptr, &inFlightFences_[i]));
    }
}

void BasicVulkanApplication::drawFrame()
{
    checkVk(vkWaitForFences(logicalDevice_, 1, &inFlightFences_[currentFrame_], VK_TRUE,
                            std::numeric_limits<uint64_t>::max()));

    // 1. Acquire image from swap chain
    uint32_t imageIndex;
    VkResult acquireResult =
        vkAcquireNextImageKHR(logicalDevice_, swapChain_, std::numeric_limits<uint64_t>::max(),
                              imageAvailableSemaphores_[currentFrame_], VK_NULL_HANDLE, &imageIndex);

    if (acquireResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
        recreateSwapChain();
        return;
    }
    else if (acquireResult == VK_SUBOPTIMAL_KHR)
    {
        // Just go on drawing, we already have the image anyway
    }
    else
    {
        checkVk(acquireResult);
    }

    updateUniformBuffer(imageIndex);

    // 2. Submit command buffer
    VkSubmitInfo submitInfo = {};
    submitInfo.sType        = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore          waitSemaphores[] = {imageAvailableSemaphores_[currentFrame_]};
    VkPipelineStageFlags waitStages[]     = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount         = 1;
    submitInfo.pWaitSemaphores            = waitSemaphores;
    submitInfo.pWaitDstStageMask          = waitStages;
    submitInfo.commandBufferCount         = 1;
    submitInfo.pCommandBuffers            = &commandBuffers_[imageIndex];

    VkSemaphore signalSemaphores[]  = {renderFinishedSemaphores_[currentFrame_]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores    = signalSemaphores;

    checkVk(vkResetFences(logicalDevice_, 1, &inFlightFences_[currentFrame_]));
    checkVk(vkQueueSubmit(graphicsQueue_, 1, &submitInfo, inFlightFences_[currentFrame_]));

    // 3. Presentation
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType            = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores    = signalSemaphores;

    VkSwapchainKHR swapChains[] = {swapChain_};
    presentInfo.swapchainCount  = 1;
    presentInfo.pSwapchains     = swapChains;
    presentInfo.pImageIndices   = &imageIndex;
    presentInfo.pResults        = nullptr; // Optional. Useful when multiple swap chains, so can check result of each

    VkResult presentResult = vkQueuePresentKHR(presentQueue_, &presentInfo);
    if (presentResult == VK_ERROR_OUT_OF_DATE_KHR || presentResult == VK_SUBOPTIMAL_KHR || hasResizedFramebuffer_)
    {
        hasResizedFramebuffer_ = false;
        recreateSwapChain();
    }
    else
    {
        checkVk(presentResult);
    }

    currentFrame_ = (currentFrame_ + 1) % MAX_FRAMES_IN_FLIGHT;
}

void BasicVulkanApplication::updateUniformBuffer(uint32_t currentImage)
{
    // Might want to use "push constants" rather than this to update values every frame

    static Util::HighResolutionTimer frameTimer;

    const float time = frameTimer.GetTimeSinceLastStartS();

    UniformBufferObject ubo = {};
    // Rotate 90 degrees per second
    ubo.modelMatrix_ = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.viewMatrix_ =
        glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.projMatrix_ =
        glm::perspective(glm::radians(45.0f), swapChainExtent_.width / (float)swapChainExtent_.height, 0.1f, 10.0f);
    ubo.projMatrix_[1][1] *= -1; // Vertical clip coord are NOT inverted in Vulkan

    void* data;
    vkMapMemory(logicalDevice_, uniformBuffersMemory_[currentImage], 0, sizeof(ubo), 0, &data);
    std::memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(logicalDevice_, uniformBuffersMemory_[currentImage]);
}

void BasicVulkanApplication::recreateSwapChain()
{
    // Handle minimization (buffer size = 0) by just waiting for size to be something else
    int width = 0, height = 0;
    while (width == 0 || height == 0)
    {
        glfwGetFramebufferSize(window_, &width, &height);
        glfwWaitEvents();
    }

    checkVk(vkDeviceWaitIdle(logicalDevice_));

    cleanupSwapChain();

    // Re-create all objects that depend on the swap chain or window size
    createSwapChain();
    createImageViews(); // Image views are based on swap chain images
    createRenderPass(); // Render pass depends on format of swap chain images (although format would rarely change)
    createGraphicsPipeline(); // Viewport + scissor size  (might use dynamic to avoid having to create again)
    createDepthResources();   // Depth buffer size = image size
    createFramebuffers();
    createCommandBuffers();
}

void BasicVulkanApplication::cleanupSwapChain()
{
    for (auto framebuffer : swapChainFramebuffers_)
    {
        vkDestroyFramebuffer(logicalDevice_, framebuffer, nullptr);
    }

    vkFreeCommandBuffers(logicalDevice_, commandPool_, static_cast<uint32_t>(commandBuffers_.size()),
                         commandBuffers_.data());

    vkDestroyPipeline(logicalDevice_, graphicsPipeline_, nullptr);
    vkDestroyPipelineLayout(logicalDevice_, pipelineLayout_, nullptr);
    vkDestroyRenderPass(logicalDevice_, renderPass_, nullptr);

    for (auto imageView : swapChainImageViews_)
    {
        vkDestroyImageView(logicalDevice_, imageView, nullptr);
    }

    vkDestroyImageView(logicalDevice_, depthImageView_, nullptr);
    vkDestroyImage(logicalDevice_, depthImage_, nullptr);
    vkFreeMemory(logicalDevice_, depthImageMemory_, nullptr);

    vkDestroySwapchainKHR(logicalDevice_, swapChain_, nullptr);
}

bool BasicVulkanApplication::isDeviceSuitable(const VkPhysicalDevice device) const
{
    if (device == VK_NULL_HANDLE)
    {
        throw std::runtime_error("Device is null");
    }

    // Get properties and features
    VkPhysicalDeviceProperties deviceProperties;
    VkPhysicalDeviceFeatures   deviceFeatures;
    vkGetPhysicalDeviceProperties(device, &deviceProperties);
    vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    const QueueFamilyIndices indices                = findSuitableQueueFamilies(device);
    const bool               areExtensionsSupported = checkDeviceExtensionSupport(device);

    bool isSwapChainAdequate = false;
    if (areExtensionsSupported)
    {
        const SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
        isSwapChainAdequate = !swapChainSupport.formats_.empty() && !swapChainSupport.presentModes_.empty();
    }

    return (indices.isComplete() && areExtensionsSupported && isSwapChainAdequate && deviceFeatures.samplerAnisotropy);
}

const BasicVulkanApplication::QueueFamilyIndices
    BasicVulkanApplication::findSuitableQueueFamilies(const VkPhysicalDevice device) const
{
    // Retrieve queue family info
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    // Find a queue that has graphics capabilities
    QueueFamilyIndices indices;
    for (uint32_t i = 0; i < queueFamilies.size(); i++)
    {
        const auto& queueFamily = queueFamilies[i];

        // Check whether this queue has graphics support
        if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices.graphicsFamily_ = i;
        }

        // Check whether the surface and queue have present support
        // We want to treat the graphics and present queues as separate for more general approach
        VkBool32 presentSupport = false;
        checkVk(vkGetPhysicalDeviceSurfaceSupportKHR(device, i, windowSurface_, &presentSupport));
        if (queueFamily.queueCount > 0 && presentSupport)
        {
            indices.presentFamily_ = i;
        }

        if (indices.isComplete())
            break;
    }

    return indices;
}

const BasicVulkanApplication::SwapChainSupportDetails
    BasicVulkanApplication::querySwapChainSupport(VkPhysicalDevice device) const
{
    SwapChainSupportDetails details;

    // Surface capabilities
    checkVk(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, windowSurface_, &details.capabilities_));

    // Surface formats
    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, windowSurface_, &formatCount, nullptr);

    if (formatCount > 0)
    {
        details.formats_.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, windowSurface_, &formatCount, details.formats_.data());
    }

    // Surface present modes
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, windowSurface_, &presentModeCount, nullptr);

    if (presentModeCount > 0)
    {
        details.presentModes_.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, windowSurface_, &presentModeCount,
                                                  details.presentModes_.data());
    }

    return details;
}

const VkSurfaceFormatKHR
    BasicVulkanApplication::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
    // No preferred format, so we simply choose what we figure is best
    if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
    {
        return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }

    // Loop through available formats and select the one we're looking for if it's there
    for (const auto& availableFormat : availableFormats)
    {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
            availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            return availableFormat;
        }
    }

    // Didn't find the one we wanted, so just take the first one
    return availableFormats[0];
}

VkPresentModeKHR
    BasicVulkanApplication::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes)
{
    // VK_PRESENT_MODE_FIFO_KHR is guaranteed to be there, so just return it if the preferred option is not available
    VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

    // Choose VK_PRESENT_MODE_MAILBOX_KHR if it's available (something like triple buffering)
    // Since VK_PRESENT_MODE_FIFO_KHR is not always *well* supported, we also prefer VK_PRESENT_MODE_IMMEDIATE_KHR
    for (const auto& availablePresentMode : availablePresentModes)
    {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            return availablePresentMode;
        }
        else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
        {
            bestMode = availablePresentMode;
        }
    }

    return bestMode;
}

const VkExtent2D BasicVulkanApplication::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
        // Window manager does not allow an extent different from the resolution of the window
        return capabilities.currentExtent;
    }
    else
    {
        // Choose the best extent that fits between minImageExtent and maxImageExtent
        glfwGetFramebufferSize(window_, &width_, &height_);
        VkExtent2D actualExtent = {static_cast<uint32_t>(width_), static_cast<uint32_t>(height_)};

        actualExtent.width  = std::max(capabilities.minImageExtent.width,
                                      std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height,
                                       std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

VkFormat BasicVulkanApplication::findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling,
                                                     VkFormatFeatureFlags features)
{
    for (VkFormat format : candidates)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physicalDevice_, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
        {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
        {
            return format;
        }
    }

    throw std::runtime_error("Failed to find a supported format!");
}

VkFormat BasicVulkanApplication::findDepthFormat()
{
    return findSupportedFormat({VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
                               VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

uint32_t BasicVulkanApplication::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice_, &memProperties);

    // Find a suitable memory type with the specified properties
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
    {
        if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
        {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable memory type!");
}

VkCommandBuffer BasicVulkanApplication::beginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool                 = commandPool_;
    allocInfo.commandBufferCount          = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(logicalDevice_, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags                    = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void BasicVulkanApplication::endSingleTimeCommands(VkCommandBuffer commandBuffer)
{
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo       = {};
    submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers    = &commandBuffer;

    vkQueueSubmit(graphicsQueue_, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphicsQueue_);

    vkFreeCommandBuffers(logicalDevice_, commandPool_, 1, &commandBuffer);
}

void BasicVulkanApplication::createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
                                         VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image,
                                         VkDeviceMemory& imageMemory)
{
    VkImageCreateInfo imageInfo = {};
    imageInfo.sType             = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType         = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width      = width;
    imageInfo.extent.height     = height;
    imageInfo.extent.depth      = 1;
    imageInfo.mipLevels         = 1;
    imageInfo.arrayLayers       = 1;
    imageInfo.format            = format;
    imageInfo.tiling            = tiling; // optimal or linear
    imageInfo.initialLayout     = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage             = usage;
    imageInfo.sharingMode       = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.samples           = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.flags             = 0; // Optional

    checkVk(vkCreateImage(logicalDevice_, &imageInfo, nullptr, &image));

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(logicalDevice_, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType                = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize       = memRequirements.size;
    allocInfo.memoryTypeIndex      = findMemoryType(memRequirements.memoryTypeBits, properties);

    checkVk(vkAllocateMemory(logicalDevice_, &allocInfo, nullptr, &imageMemory));
    checkVk(vkBindImageMemory(logicalDevice_, image, imageMemory, 0));
}

void BasicVulkanApplication::transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout,
                                                   VkImageLayout newLayout)
{
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkImageMemoryBarrier barrier = {};
    barrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout            = oldLayout;
    barrier.newLayout            = newLayout;

    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.image                           = image;
    barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel   = 0;
    barrier.subresourceRange.levelCount     = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount     = 1;

    barrier.srcAccessMask = 0; // TODO
    barrier.dstAccessMask = 0; // TODO

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        if (hasStencilComponent(format))
        {
            barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    }
    else
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    // Set the proper barrier masks
    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage      = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage      = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask =
            VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        sourceStage      = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else
    {
        throw std::invalid_argument("Unsupported layout transition!");
    }

    vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

    endSingleTimeCommands(commandBuffer);
}

VkImageView BasicVulkanApplication::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo viewInfo           = {};
    viewInfo.sType                           = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image                           = image;
    viewInfo.viewType                        = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format                          = format;
    viewInfo.subresourceRange.aspectMask     = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel   = 0;
    viewInfo.subresourceRange.levelCount     = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount     = 1;

    VkImageView imageView;
    checkVk(vkCreateImageView(logicalDevice_, &viewInfo, nullptr, &imageView));

    return imageView;
}

void BasicVulkanApplication::createBuffer(const VkDeviceSize size, const VkBufferUsageFlags usage,
                                          const VkMemoryPropertyFlags properties, VkBuffer& buffer,
                                          VkDeviceMemory& bufferMemory)
{
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType              = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size               = size;
    bufferInfo.usage              = usage;
    bufferInfo.sharingMode        = VK_SHARING_MODE_EXCLUSIVE;

    checkVk(vkCreateBuffer(logicalDevice_, &bufferInfo, nullptr, &buffer));

    // We have the buffer, now we need to allocate memory for it (WTF?)
    // Get requirements first
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(logicalDevice_, buffer, &memRequirements);

    // Then allocate the memory
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType                = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize       = memRequirements.size;
    allocInfo.memoryTypeIndex      = findMemoryType(memRequirements.memoryTypeBits, properties);

    checkVk(vkAllocateMemory(logicalDevice_, &allocInfo, nullptr, &bufferMemory));

    // Then bind it to the buffer
    checkVk(vkBindBufferMemory(logicalDevice_, buffer, bufferMemory, 0));
}

void BasicVulkanApplication::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset    = 0; // Optional
    copyRegion.dstOffset    = 0; // Optional
    copyRegion.size         = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    endSingleTimeCommands(commandBuffer);
}

void BasicVulkanApplication::copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();

    VkBufferImageCopy region = {};
    region.bufferOffset      = 0;
    region.bufferRowLength   = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel       = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount     = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    endSingleTimeCommands(commandBuffer);
}

VkShaderModule BasicVulkanApplication::createShaderModule(const std::vector<char>& shaderCode)
{
    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType                    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize                 = shaderCode.size();
    createInfo.pCode                    = reinterpret_cast<const uint32_t*>(shaderCode.data());

    VkShaderModule shaderModule;
    checkVk(vkCreateShaderModule(logicalDevice_, &createInfo, nullptr, &shaderModule));

    return shaderModule;
}

bool BasicVulkanApplication::checkValidationLayerSupport()
{
    // Find out how many layers
    uint32_t layerCount;
    checkVk(vkEnumerateInstanceLayerProperties(&layerCount, nullptr));

    // Get the layers
    std::vector<VkLayerProperties> availableLayers(layerCount);
    checkVk(vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data()));

    for (const char* layerName : validationLayers)
    {
        bool layerFound = false;

        for (const auto& layerProperties : availableLayers)
        {
            if (strcmp(layerName, layerProperties.layerName) == 0)
            {
                layerFound = true;
                break;
            }
        }

        if (!layerFound)
        {
            return false;
        }
    }

    return true;
}

bool BasicVulkanApplication::checkDeviceExtensionSupport(const VkPhysicalDevice device)
{
    // Get list of available extensions
    uint32_t extensionCount;
    checkVk(vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr));

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    checkVk(vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data()));

    // Compile set of required extensions
    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

    // Remove available extensions from the set of required ones, until there are no unsatisfied required extensions
    for (const auto& extension : availableExtensions)
    {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}

std::vector<const char*> BasicVulkanApplication::getRequiredExtensions()
{
    uint32_t     glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if (enableValidationLayers)
    {
        extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    return extensions;
}

VkBool32 BasicVulkanApplication::debugCallback(VkDebugReportFlagsEXT /* flags */,
                                               VkDebugReportObjectTypeEXT /* objType */, uint64_t /* obj */,
                                               size_t /* location */, int32_t /* code */, const char* /* layerPrefix */,
                                               const char* msg, void* /* userData */)
{
    std::cerr << "[Validation layer] " << msg << std::endl;
    return VK_FALSE;
}

VkResult BasicVulkanApplication::createDebugReportCallbackEXT(VkInstance                                instance,
                                                              const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
                                                              const VkAllocationCallbacks*              pAllocator,
                                                              VkDebugReportCallbackEXT*                 pCallback)
{
    auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    if (func != nullptr)
    {
        // Call and return its return value
        return func(instance, pCreateInfo, pAllocator, pCallback);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void BasicVulkanApplication::destroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback,
                                                           const VkAllocationCallbacks* pAllocator)
{
    auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    if (func != nullptr)
    {
        // Call it
        func(instance, callback, pAllocator);
    }
}

void BasicVulkanApplication::framebufferResizeCallback(GLFWwindow* window, int /* width */, int /* height */)
{
    // Simply set the resized fb flag to true. Application will take care of the rest
    auto app                    = reinterpret_cast<BasicVulkanApplication*>(glfwGetWindowUserPointer(window));
    app->hasResizedFramebuffer_ = true;
}
